from sympy import *
from sympy.matrices import *
import subprocess
from sympy import latex
import sys



p=Symbol("p")
rho=Symbol("rho")
phi=Symbol("phi")

lam=Matrix([[1,0,0,0],[0,cos(phi),sin(phi),0],[0,-sin(phi),cos(phi),0],[0,0,0,1]])

pprint(S(lam*lam.T))

T=Matrix([[rho,0,0,0],[0,p,0,0],[0,0,p,0],[0,0,0,p]])

a=lam*T*lam.T
a=S(a)
pprint(a)
pprint(S(lam*T-T*lam))