import numpy as np
import matplotlib.pyplot as plt
from sympy import *
import scipy.optimize as so
import sys
import subprocess


def Sp(x):
  return S(factor(S(expand(S(x)))))

def Sq(x):
  return Sp(trigsimp(Sp(x)))

i = Matrix([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
# g = Matrix([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,-1]])
# g0 = Matrix([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
# g1 = Matrix([[0,0,0,1],[0,0,1,0],[0,-1,0,0],[-1,0,0,0]])
# g2 = Matrix([[0,0,0,-I],[0,0,I,0],[0,I,0,0],[-I,0,0,0]])
# g3 = Matrix([[0,0,1,0],[0,0,0,-1],[-1,0,0,0],[0,1,0,0]])
# k1 = Matrix([[0,1,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0]])
# k2 = Matrix([[0,0,1,0],[0,0,0,0],[1,0,0,0],[0,0,0,0]])
# k3 = Matrix([[0,0,0,1],[0,0,0,0],[0,0,0,0],[1,0,0,0]])
# j1 = Matrix([[0,0,0,0],[0,0,0,0],[0,0,0,-1],[0,0,1,0]])
# j2 = Matrix([[0,0,0,0],[0,0,0,1],[0,0,0,0],[0,-1,0,0]])
# j3 = Matrix([[0,0,0,0],[0,0,-1,0],[0,1,0,0],[0,0,0,0]])



chi=Symbol("chi")
phi=Symbol("phi")
theta=Symbol("theta")
R=Symbol("R")

x=R*sin(chi)*sin(theta)*cos(phi)
y=R*sin(chi)*sin(theta)*sin(phi)
z=R*sin(chi)*cos(theta)
w=R*cos(chi)

a=[x,y,z,w]
b=[R,chi,phi,theta]


M=i*i

j0=0

for j in range(4):
  for jj in range(4):
    M[j0]=diff(a[j],b[jj])
    j0+=1
pprint(M)
pprint(Sq(M.det()))


md=M.det()
md2=Sq(md)

filename = "output"
documentString = r'''\documentclass[landscape]{article}
\usepackage[utf8]{inputenc}
\usepackage{physics}
\usepackage[landscape]{geometry}
\usepackage{amsmath}
\begin{document}
    \scrollmode
'''

documentString+=r"\begin{align}"+latex(M)+"\end{align}"
documentString+=r"\begin{align}"+latex(md)+"\end{align}"
documentString+=r"\begin{align}"+latex(md2)+"\end{align}"


documentString += r'''
\batchmode
\end{document}'''
filename="doc"
with open(filename + ".tex", "w") as tex_file:
    tex_file.write(documentString)

print("\n\nCreate LaTeX output...")
subprocess.call(["pdflatex", "-interaction=batchmode", filename + ".tex"])
print("Output was created as " + filename + ".pdf")



