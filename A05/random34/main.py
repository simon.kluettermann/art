import numpy as np
import matplotlib.pyplot as plt
from sympy import latex
from sympy import *
import scipy.optimize as so
import sys
import subprocess


i = Matrix([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
g0 = Matrix([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
g1 = Matrix([[0,0,0,1],[0,0,1,0],[0,-1,0,0],[-1,0,0,0]])
g2 = Matrix([[0,0,0,-I],[0,0,I,0],[0,I,0,0],[-I,0,0,0]])
g3 = Matrix([[0,0,1,0],[0,0,0,-1],[-1,0,0,0],[0,1,0,0]])
n = Matrix([[-1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])

a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16=symbols("a:4(:4)")
Ma=Matrix([[a1,a2,a3,a4],[a5,a6,a7,a8],[a9,a10,a11,a12],[a13,a14,a15,a16]])
b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16=symbols("b:4(:4)")
Mb=Matrix([[b1,b2,b3,b4],[b5,b6,b7,b8],[b9,b10,b11,b12],[b13,b14,b15,b16]])
c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16=symbols("c:4(:4)")
Na=Matrix([[c1,c2,c3,c4],[c5,c6,c7,c8],[c9,c10,c11,c12],[c13,c14,c15,c16]])
d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16=symbols("d:4(:4)")
Nb=Matrix([[d1,d2,d3,d4],[d5,d6,d7,d8],[d9,d10,d11,d12],[d13,d14,d15,d16]])




Phi=Symbol('\Phi')
g=n-(2*Phi*i)
gi=Matrix([[-1/(1+2*Phi),0,0,0],[0,1/(1-2*Phi),0,0],[0,0,1/(1-2*Phi),0],[0,0,0,1/(1-2*Phi)]])
gis=Matrix([[-(1-2*Phi),0,0,0],[0,(1+2*Phi),0,0],[0,0,(1+2*Phi),0],[0,0,0,(1+2*Phi)]])

##simplify a tat
g=n-(Phi*i)
gi=Matrix([[-1/(1+Phi),0,0,0],[0,1/(1-Phi),0,0],[0,0,1/(1-Phi),0],[0,0,0,1/(1-Phi)]])
gis=Matrix([[-(1-Phi),0,0,0],[0,(1+Phi),0,0],[0,0,(1+Phi),0],[0,0,0,(1+Phi)]])


##end simp


swap=Matrix([[1,0,0,0],[0,0,1,0],[0,1,0,0],[0,0,0,1]])

M=Ma+(Phi*Mb)

def simpe(x):
  x=S(expand(x))
  return S(factor(S(x.replace(Phi,0)+diff(x,Phi).replace(Phi,0)*Phi)))
def p0(x):
  x=S(expand(x))
  return S(factor(S(x.replace(Phi,0))))
def p1(x):
  x=S(expand(x))
  return S(factor(S(diff(x,Phi).replace(Phi,0))))


KK=M.T*g*M*n-i
KK=simpe(KK)
# pprint(KK)

# KK2=g*M*n-i
# KK2=simpe(KK2)
# pprint(KK2)

# KK3=KK-KK2
# KK3=simpe(KK3)
# pprint(KK3)
# print("Hello")
# pprint(KK[0])


eq=[]
par=[]
for i in range(16):
  eq.append(p0(KK[i]))
  eq.append(p1(KK[i]))
  par.append(eval("a"+str(i+1)))
  par.append(eval("b"+str(i+1)))

# print("Now solving...")
# sol=solve(eq,par,quick=True)#manual=True
# # sol=nsolve(eq,par,np.arange(0,1,1/32))##whats wrong?
# pprint("Solved to...")
# pprint(sol)
# print(len(sol))
  
# sys.exit()

##testing range
trivial=n*gi
test=S(Ma*trivial*Mb)


pprint(swap.T*g*gi*n*swap)
# pprint(S(test*g))


# pprint(trivial)
# pprint(test)


sys.exit()

##teching range
filename = "output"
documentString = r'''\documentclass[landscape]{article}
\usepackage[utf8]{inputenc}
\usepackage{physics}
\usepackage[landscape]{geometry}
\usepackage{amsmath}
\begin{document}
    \scrollmode
    \title{Title}\author{}
    \maketitle
'''
# documentString += r"k1:\begin{align}"+latex(KK)+"\end{align}"
# documentString += r"k2:\begin{align}"+latex(KK2)+"\end{align}"
# documentString += r"k3:\begin{align}"+latex(KK3)+"\end{align}"




for i in range(16):
  documentString += str("zero"+str(i)+":")
  documentString += r"\begin{align}"+latex(p0(KK[i]))+"\end{align}"
  documentString += str("one"+str(i)+":")
  documentString += r"\begin{align}"+latex(p1(KK[i]))+"\end{align}"



documentString += r'''
\batchmode
\end{document}'''
filename="doc"
with open(filename + ".tex", "w") as tex_file:
    tex_file.write(documentString)

print("\n\nCreate LaTeX output...")
subprocess.call(["pdflatex", "-interaction=batchmode", filename + ".tex"])
print("Output was created as " + filename + ".pdf")

